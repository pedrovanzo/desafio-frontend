import React from 'react'
import './App.css'
import './custom.scss'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Main from './pages/main'
// import 'bootstrap/dist/css/bootstrap.min.css'


function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={Main} />
        </Switch>
      </Router>
    </>
  )
}

export default App
