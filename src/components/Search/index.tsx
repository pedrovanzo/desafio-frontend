import React, { useCallback, useRef, useState } from 'react'
import { Button, Form, FormControl, InputGroup, Navbar } from 'react-bootstrap'
import gitSearchLogoPath from '../../assets/logo/df-logo-sm.png'
import SearchIcon from '../../assets/icons/search.png'
import useSearchUser from '../../pages/search/useSearchUser'
import LogoPlaceholder from '../LogoPlaceholder'
import DisplaySearchResults from '../DisplaySearchResults'

export default function SearchComponent() {

    const searchInputRef = useRef() as React.MutableRefObject<HTMLInputElement>

    const [query, setQuery] = useState('')
    const [pageNumber, setPageNumber] = useState(1)

    // import search data
    const {
        usersData,
        hasMore,
        loading,
    } = useSearchUser(query, pageNumber)

    // Check if element is last on the list
    const observer: any = useRef()
    const lastUserElementRef = useCallback(node => {
        if (loading) return
        if (observer.current) observer.current.disconnect()
        observer.current = new IntersectionObserver(entries => {
            if (entries[0].isIntersecting && hasMore) {
                setPageNumber(prevPageNumber => prevPageNumber + 1)
            }
        })
        if (node) observer.current.observe(node)
    }, [loading, hasMore])

    function handleSearch(e: any) {
        setQuery(e)
        setPageNumber(1)
    }

    return (
        <>

            <Navbar bg="white" expand="lg" className="navigation">
                <Navbar.Brand href="/">
                    <img
                        src={gitSearchLogoPath}
                        className="d-inline-block align-top mx-3 gitSearchSmLogo"
                        alt="The Git Search"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll" className="d-flex justify-content-center">
                    <Form className="d-flex my-2 mx-3 w-100" onSubmit={(e) => {
                        e.preventDefault()
                        handleSearch(searchInputRef.current.value)
                    }}>
                        <InputGroup>
                            <FormControl id="inlineFormInputGroup" ref={searchInputRef} placeholder="Pesquisar" />

                            <Button className="d-flex justify-content-center align-items-center" type="submit">
                                <img src={SearchIcon} alt="" className="searchIcon" style={{ width: '.9rem' }} />
                            </Button>
                        </InputGroup>
                    </Form>
                </Navbar.Collapse>
            </Navbar>
            {searchInputRef?.current?.value ?
                // If there is a search, load search component
                <>
                    <DisplaySearchResults usersDataProps={{
                        query,
                        usersData,
                        loading,
                        lastUserElementRef
                    }} key={usersData.login} />
                </>
                :
                // Else, display search logo
                <LogoPlaceholder key={usersData.login} />
            }
        </>
    )
}
