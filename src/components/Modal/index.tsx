import React from 'react'
import { Button, Modal } from 'react-bootstrap'
import './index.css'

export default function ModalComponent(props: any) {

    let userData = props.userObjectData
    const createdAtAux = new Date(userData.created_at)
    const createdAt = createdAtAux.getDay() + '/' + createdAtAux.getMonth() + '/' + createdAtAux.getFullYear()
    
    return (
        <>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                >
                <div className="modal-grid-container py-4 px-3">
                        <div className="d-flex align-items-center justify-content-center px-1 modal-avatar-container mx-auto my-2">
                            <img src={userData.avatar_url} alt="" className="modal-avatar" />
                        </div>
                        <div className="px-2 py-2" style={{ wordBreak: 'break-word', lineHeight: '22px' }}>
                            <div className="modal-title text-dark">
                                {userData.name}
                            </div>
                            <hr className="my-1" />
                            <br />
                            <div className="d-flex">
                                <div className="flex-fill">
                                    <span className="modal-subtitle-label text-dark">Username:</span> <br />
                                    <span className="modal-subtitle-data text-light">{userData.login}</span>
                                </div>
                                <div>
                                    <span className="modal-subtitle-label text-dark">Seguindo:</span> <br />
                                    <span className="modal-subtitle-data text-light" style={{ float: 'right' }}>{userData.following}</span>
                                </div>
                            </div>
                            <div className="d-flex">
                                <div className="flex-fill">
                                    <span className="modal-subtitle-label text-dark">Cadastrado(a):</span> <br />
                                    <span className="modal-subtitle-data text-light">{createdAt}</span>
                                </div>
                                <div>
                                    <span className="modal-subtitle-label text-dark">Seguidores:</span> <br />
                                    <span className="modal-subtitle-data text-light" style={{ float: 'right' }}>{userData.followers}</span>
                                </div>
                            </div>
                            <br />
                            <div className="d-flex justify-content-between">
                                <div>
                                    <span className="modal-subtitle-label text-dark">URL:</span> <br />
                                    <span className="modal-subtitle-data text-light">{userData.html_url}</span>
                                </div>
                            </div>
                                <div style={{ float: 'right' }}>
                                    <Button onClick={props.onHide} variant="outline-primary">Fechar</Button>
                                </div>
                        </div>
                    </div>
            </Modal>
        </>
    )
}
