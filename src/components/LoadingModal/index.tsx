import React from 'react'
import { Modal } from 'react-bootstrap'

export default function LoadingModal(props: any) {

    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            className="d-flex justify-content-center align-items-center"
        >
            <div className="px-4 py-4">
                Carregando...
            </div>
        </Modal>
    )
}
