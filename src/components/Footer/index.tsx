import React from 'react'
import { Navbar } from 'react-bootstrap'

export default function FooterComponent() {
    return (
        <>
            <div className="fixed-bottom">
                <Navbar bg="white" className="justify-content-center" style={{ fontWeight: 300, fontSize: '14px' }}>
                    Projetado por: Pedro Vanzo - 20/10/2021
                </Navbar>
            </div>
        </>
    )
}
