import React, { useState } from 'react'
import axios from 'axios'
import { Button, Col } from 'react-bootstrap'
import LoadingModal from '../LoadingModal'
import ModalComponent from '../Modal'

export default function UserDataCard(props: any) {

    const { userDataProps } = props

    const [modalShow, setModalShow] = useState(false)
    const [loadingModalShow, setLoadingModalShow] = useState(false)
    const [userObjectData, setUserObjectData]: any = useState({})

    // Get individual user's data
    function getUserDetailsAndOpenModal(user: any) {
        setLoadingModalShow(true)
        axios({
            method: 'GET',
            url: 'https://api.github.com/users/' + user.toString(),
        }).then(res => {
            // Data to state
            setUserObjectData(res.data)
            setLoadingModalShow(false)
            setModalShow(true)
        })

    }

    return (
        <>
            <LoadingModal
                show={loadingModalShow}
                onHide={() => setLoadingModalShow(false)}
            />
            <ModalComponent
                show={modalShow}
                onHide={() => setModalShow(false)}
                userObjectData={userObjectData}
            />
            <Col ref={props.elementRef} key={userDataProps.index} className="text-center my-3" md={3} sm={6}>
                <div className="mx-auto" style={{ maxWidth: '230px', backgroundColor: 'white', boxShadow: '0px 0px 6px hsla(259, 30%, 21%, .25)', borderRadius: '10px', overflow: 'clip', wordBreak: 'break-word' }}>
                    <div>
                        <img src={userDataProps.avatar_url} alt="" style={{ width: '100%', height: '130px', objectFit: 'cover' }} />
                    </div>
                    <div className="pt-3 px-2 text-dark" style={{ textAlign: 'left' }}>
                        {userDataProps.login}
                    </div>
                    <div className="px-2" style={{ textAlign: 'left', fontSize: '12px' }}>
                        <a href={userDataProps.html_url} target="_blank" rel="noreferrer" className="text-secondary">{userDataProps.html_url}</a>
                    </div>
                    <div className="px-2 text-primary" style={{ textAlign: 'left', fontSize: '12px' }}>
                        Score: {parseFloat(userDataProps.score).toFixed(2)}
                    </div>
                    <div>
                        <Button onClick={() => {
                            getUserDetailsAndOpenModal(userDataProps.login)
                        }} className="my-3 mx-1 text-uppercase" style={{ borderRadius: '50px', fontSize: '.9rem', width: '90%' }}>
                            Ver Mais
                        </Button>
                    </div>
                </div>
            </Col>
        </>
    )
}
