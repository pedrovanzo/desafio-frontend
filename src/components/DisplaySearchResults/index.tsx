import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import UserDataCard from '../Card'
import Loading from '../Loading'
import './index.css'

export default function DisplaySearchResults(props: any) {

    const { usersDataProps } = props

    return (
        <>
            <Container style={{ backgroundImage: `url(/backgroung-full.png)`, backgroundSize: '100%', width: '100%', minHeight: '80vh' }} fluid className="cards-container mx-0 my-0 pb-5">
                <Row className="d-flex mx-auto mt-3">
                    <Col className="px-5 py-2">
                        Resultados para: <b>{usersDataProps.query}</b>
                    </Col>
                </Row>
                <Row className="d-flex justify-content-center mx-auto">
                    {usersDataProps.usersData.map((item: any, index: any) => {
                        if (usersDataProps.usersData.length === index + 1) {
                            return (
                                <>
                                    <UserDataCard userDataProps={item} elementRef={usersDataProps.lastUserElementRef} />
                                </>
                            )
                        } else {
                            return (
                                <>
                                    <UserDataCard userDataProps={item} />
                                </>
                            )
                        }
                    })}
                    <div>{usersDataProps.loading && <Loading />}</div>
                </Row>
            </Container>
        </>
    )
}
