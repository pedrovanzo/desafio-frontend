import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import gitSearchLogoPath from '../../assets/logo/df-logo-lg.png'

export default function LogoPlaceholder() {
    return (
        <>
            {/* Content */}
            <Container fluid>
                <Row className="d-flex align-items-center justify-content-center" style={{ height: '80vh' }}>
                    <Col className="text-center">
                        <img src={gitSearchLogoPath} alt="The Git Search" className="gitSearchLgLogo" />
                    </Col>
                </Row>
            </Container>
        </>
    )
}
