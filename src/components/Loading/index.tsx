import React from 'react'
import loadingGifPath from '../../assets/gifs/eater.gif'

export default function Loading() {
    return (
        <div className="w-100 text-center">
            <img src={loadingGifPath} alt="" style={{ width: '3rem' }}/>
        </div>
    )
}
