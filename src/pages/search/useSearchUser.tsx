import { useEffect, useState } from 'react'
import axios from 'axios'

export default function useSearchUser(query: any, pageNumber: any) {
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(false)
    const [usersData, setusersData]: any = useState([])
    const [hasMore, setHasMore] = useState(false)

    useEffect(() => {
        setusersData([])
    }, [query])

    useEffect(() => {
        if(query){
            setLoading(true)
        setError(false)
        let cancel:any
        // Get data from API
        axios({
            method: 'GET',
            url: 'https://api.github.com/search/users',
              params: { q: query, page: pageNumber },
            cancelToken: new axios.CancelToken(c => cancel = c)

        }).then((res: any) => {
            // Data to state
            setusersData((prevUsersData: any) => {
                return [...new Set([...prevUsersData, ...res.data.items])]
            })
            // Has next batch of users
            setHasMore(res.data.items.length > 0)
            setLoading(false)
        }).catch((e: any) => {
            // On empty search or other request, cancel current request
            if (axios.isCancel(e)) return
            setError(true)
          })
          return () => cancel()
        }
    }, [query, pageNumber])

    return { loading, error, usersData, hasMore }
}
