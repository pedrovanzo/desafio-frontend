import React from 'react'
import SearchComponent from '../../components/Search'
import FooterComponent from '../../components/Footer'

export default function Main() {
    return (
        <>
            {/* Navigation */}
            <SearchComponent />
            {/* Footer */}
            <FooterComponent />
        </>
    )
}
